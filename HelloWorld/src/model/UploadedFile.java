package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Entity
@Table(name = "UPLOADED_FILES")
public class UploadedFile implements Serializable {
	private static final long serialVersionUID = -5559139134122091763L;
	@Id
	@TableGenerator(name = "genUploadedFile", table = "ID_GEN", pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE", pkColumnValue = "ID_UPLOADED_FILE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "genUploadedFile")
	@Column(name = "ID_UPLOADED_FILE")
	private Long id;

	@Column(name = "FILE_NAME")
	private String fileName;

	@Column(name = "CONTENT_TYPE")
	private String contentType;

	@Column(name = "CONTENT")
	@Lob
	private byte[] content;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public byte[] getContent() {
		if (content == null) {
			return null;
		}
		return content.clone();
	}

	public void setContent(byte[] content) {
		if (content == null) {
			this.content = null;
		}
		this.content = content.clone();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.id).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		UploadedFile otherObject = (UploadedFile) obj;
		return new EqualsBuilder().append(this.id, otherObject.id).isEquals();
	}
}