package service;

import model.UploadedFile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * @author mcristea
 * 
 */
@Transactional
@Service("uploadedFileService")
public class UploadedFileServiceImpl implements UploadedFileService {

	@PersistenceContext
	private EntityManager em;

	@Override
	public UploadedFile getUploadedFileById(Long id) {
		return em.find(UploadedFile.class, id);
	}

	@Override
	public Long saveUploadedFile(UploadedFile uploadedFile) {
		em.persist(uploadedFile);
		return uploadedFile.getId();
	}
}