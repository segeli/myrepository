package service;

import model.UploadedFile;

public interface UploadedFileService {
	UploadedFile getUploadedFileById(Long id);

	Long saveUploadedFile(UploadedFile uploadedFile);
}
